// Bài 1: Tính tiền lương nhân viên
/*
Đầu vào: Số ngày làm
Xử lí:
-Bước 1:Khai báo và gán giá trị cho lương một ngày 
-Bước 2: Gán lương= lương 1 ngày * số ngày làm
-Bước 3:in kết quả
Đầu ra: Lương
*/
var luongMotNgay = 100000;
console.log('Lương một ngày:', luongMotNgay);
var soNgayLam = 5;
console.log('Số ngày làm:', soNgayLam);
var luong = soNgayLam * luongMotNgay;
console.log('Lương của nhân viên là:', luong);

// Bài 2: Tính giá trị trung bình
/*
Đầu vào: 5 số thực
Xử lí:
-Bước 1; Nhập và gán giá trị cho 5 số Thực
-bước 2: giá trị trung bình= tổng 5 số thực /5
Đầu ra: Giá trị trung bình
*/
var soThuNhat = 1;
var soThuHai = 2;
var soThuBa = 3;
var soThuTu = 4;
var soThuNam = 5;
var giaTriTrungBinh = (soThuNhat + soThuHai + soThuBa + soThuTu + soThuNam) / 5;
console.log('Giá trị trung bình:', giaTriTrungBinh);

// Bài 3: Quy đổi tiền
/**
 * Đầu vào:Số tiền USD
 * Xử lí:
 * Bước 1: Nhập vào số tiền bằng mệnh giá USD
 * Bước 2: Quy đổi ra VND bằng công thức:1USD=23.500VND
 *
 * Đầu ra:Số tiền VND
 */

var tienDo = 2;
let tienQuyDoi;
console.log('Số tiền USD:', tienDo);
tienQuyDoi = tienDo * 235000;
console.log('Tiền quy đổi sang VND:', tienQuyDoi, 'VND');

// Bài 4:Tính diện tích, chu vi hình chữ nhật
/**
 * Đầu vào: Chiều dài, chiều rộng
 * Xử lí:
 * Bước 1:Khai báo biến chứa chiều dài và chiều rộng
 * Bước 2: Khai báo biến chứa chu vi và diện tích
 * Bước 3: Gán chu vi=(chiều dài+chiều rộng)*2
 * Bước 4:Gán diện tích=(chiều dài*chiều rộng)
 * Bước 5 :Xuất kết quả
 *
 * Đầu ra: Diện tích, chu vi
 */
var chieuDai = 5;
console.log('chiều dài:', chieuDai);
var chieuRong = 4;
console.log('chiều rộng:', chieuRong);
var chuVi = (chieuDai + chieuRong) * 2;
var dienTich = chieuDai * chieuRong;
console.log('Chu vi:', chuVi);
console.log('Diện tích', dienTich);

// Bài 5: Tính tổng 2 kí số
/*
Đầu vào: Số thực có 2 chữ số
Xử lí:
-Bước 1: Khai báo và nhập giá trị cho số có 2 chữ số
-Bước 2: gán chữ số hàng đơn vị=n%10
-bước 3:gán chữ số hàng chục=n/10
-bước 4:Gán tổng=chữ số hàng đơn vị+chữ số hàng chục
Đầu ra: Tổng 2 kí số
 */
var a = 26;
let tongHaiKySo = 0;
var hangDonVi = a % 10;
console.log('hàng đơn vị:', hangDonVi);
var hangChuc = Math.floor(a / 10);
console.log('hàng chục:', hangChuc);
tongHaiKySo = hangDonVi + hangChuc;
console.log('Tổng', tongHaiKySo);